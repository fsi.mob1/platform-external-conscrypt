Finch-Platform: AOSP platform/external/conscrypt mod
====================================================

## NOTICE

This project is part of [Finch](https://gitlab.com/fsi.mob1/finch); please
see earlier link for entire project description.

We may missed some requirements such as mentioning correct authors of our
dependent library, and so on. We never tries to violate other's, so if you
found mistake please feel free to contact us and we will gladly correct them.

[Original README is here](README_orig.md).

## AUTHORS

Original works from https://android.googlesource.com/platform/external/conscrypt .

Modification tracked in this repository are made by belowing author(s):

- SungHyoun Song <decash@fsec.or.kr>

## LICENSE

Apache 2.0 (Same as origin). See [LICENSE](LICENSE) for details.

You can see modification by diffing between commit `bb2fc1d01713306cfc0d1cf50fcbe16bc1b8c838` and HEAD.
